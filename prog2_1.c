#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <gmp.h>
#include "prog2_1.h"

TSAFELIST *tSafeConstruct()
{
    TSAFELIST *list = malloc(sizeof(TSAFELIST));
    list->mutex = malloc(sizeof(pthread_mutex_t)); 
    pthread_mutex_init(list->mutex, NULL);
    list->head = NULL;
    return list;              
}


void tSafeDestruct(TSAFELIST *list)      // free the memory used for the list   
{
    free(list);    
}


void tSafeEnqueue(TSAFELIST *list, mpz_t newEntry)
{
    pthread_mutex_lock(list->mutex); 

    if(list->head == NULL)    // list has no elements
    {
        TSAFENODE *newNode = malloc(sizeof(TSAFENODE));
        mpz_init(newNode->number); 

        list->head = newNode;                      // the new node becomes the head
        mpz_set(newNode->number, newEntry);        // set the head value to the new entry
        newNode->next = NULL;                      // since there's only one node(the head), next is null 
    }   
    else
    {
        TSAFENODE *newNode = malloc(sizeof(TSAFENODE));
        mpz_init(newNode->number); 
        TSAFENODE *currentNode;
        currentNode = list->head;           // first set the current node to the head
        while(currentNode->next != NULL)    // while next node isn't NULL, go to next node
        {
            currentNode = currentNode->next;
        }
     
        currentNode->next = newNode;        // on last node, set next value to new node
        mpz_set(newNode->number, newEntry);             // set number value of the new node 
        newNode->next = NULL;                           // set the next value of the new node
    }                                     
    pthread_mutex_unlock(list->mutex);
}



TSAFEDATA tSafeDequeue(TSAFELIST *list)
{
    pthread_mutex_lock(list->mutex); 
    TSAFEDATA returnValue;
    
    if(list->head == NULL)    
    {
        
        returnValue.isValid = 0;        
        mpz_init(returnValue.value);
        pthread_mutex_unlock(list->mutex);
        return returnValue;                         // return entire struct    
    }
      
    else                                            // more than one element in the list
    {
 
        returnValue.isValid = 1;        
        mpz_init(returnValue.value);        
        mpz_set(returnValue.value,list->head->number);
        list->head = list->head->next;              // garbage collection of the head
        pthread_mutex_unlock(list->mutex);
        return returnValue; 
    }
}


