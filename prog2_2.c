#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <gmp.h>
#include "prog2_1.c"
#include <math.h>

// global variables
// Program should startup by initializing globalCounter & num

mpz_t globalCounter;
pthread_mutex_t counterguard = PTHREAD_MUTEX_INITIALIZER;
TSAFELIST *numberList;
int K = 0; 

void *generatePrimes(void *ptr)
{
    int test = 0; 
    while(test < K)
    {
        mpz_t count; 
        mpz_init(count);
 
        pthread_mutex_lock(&counterguard);
        
        mpz_set(count, globalCounter);
        mpz_add_ui(globalCounter, globalCounter, 2);        // adds 1 to the counter 

        pthread_mutex_unlock(&counterguard);
        

        int returnValue = mpz_probab_prime_p(count, 100000);                        
        if(returnValue > 0)   // if the number is pseudoprime
        {
            tSafeEnqueue(numberList, count);
            test++;
        }   
    }  
}

int main(int argc, char *argv[])
{
    printf("Assignment#2-2, Travis Dattilo, travisdattilo@yahoo.com\n");
    if(argc != 3)
    {
        printf("\nThis program expects two command line arguments.\n"); 
        return 0;
    }
   
    K = atoi(argv[1]);     
    int B = atoi(argv[2]); 
    int howManyPrimes = 0; 
    int status;

    mpz_init(globalCounter);
    numberList = tSafeConstruct();      
    mpz_ui_pow_ui(globalCounter, 2, B); 
    mpz_add_ui(globalCounter,globalCounter,1);
    
    pthread_t myThread; 
    for(int i = 0; i < 4; i++)  // start 4 threads running the generatePrimes function 
    {
        status = pthread_create(&myThread,NULL,generatePrimes,NULL);
        if(status != 0)
        {
            printf("Error creating thread %d\n",i);
            return 0;
        }
    }
    
    while(howManyPrimes < K)   // while we are still getting primes
    {
        TSAFEDATA primeNum = tSafeDequeue(numberList);  
        if(primeNum.isValid == 1)                       
        {
            gmp_printf("%Zd\n",primeNum.value);    
            howManyPrimes++;
        }
        else
        {
            sleep(1);  
        }
    }    

}








