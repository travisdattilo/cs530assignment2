Travis Dattilo  
travisdattilo@yahoo.com  

"prog2_1.h"  
Header file with struct and methods to implement in prog2_1 to create a thread safe
linked list queue. 

"prog2_1.c"  
Implementation of a thread safe linked list queue using the prog2_1.h header file. 
Contains a tSafeConstruct method that is the constructor for the linked list 
queue. There is a tSafeDestruct that will free up memory space for a previously 
allocated linked list. The tSafeEnqueue method will enqueue a new node into
the linked list queue. The enqueue method checks for the following cases 
including the following: if there are no nodes in the list then a new entry 
will be made the head of the linked list, or if there are one or more nodes
in the list then it will traverse the list starting at the head to the last
node in the list and add it after that. The last method, tSafeDequeue, will 
remove the head node from the list and make the next node after that the head.
This is to ensure the linked list queue uses a FIFO (first in first out) 
mechanism. 

"prog2_2.c"  
This C program acts as a thread safe pseudoprime number generator. The user will 
compile the program with "gcc prog2_2.c -o prog2_2 -l gmp -pthread" then run the 
program with "./prog2_2 K B" where instead of typing K, type the the number of 
pseudoprime numbers you want generated and instead of B, type the desired bitlength 
of each number that will be generated. The numbers will be printed to STDOUT each 
on their own line. 
